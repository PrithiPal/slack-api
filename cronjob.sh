#!/bin/bash


SLACK_DIR=/home/pi/slack-api

echo "python3 test_functions.py .."
python3 stembot/test_functions.py
if [ $? -eq 0 ]; then
    cd ${SLACK_DIR}

    echo "Pushing code.. "
    git checkout analysis 
    git add . 
    git commit -m "Updated analysis"
    git push origin analysis
else
    echo "Problem with push. Aborting.."
fi
